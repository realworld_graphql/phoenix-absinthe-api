# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :real_world,
  ecto_repos: [RealWorld.Repo]

# Configures the endpoint
config :real_world, RealWorldWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MKRHMtzkKAed0GCv025ZTp0x6ghDtQFIp6vFlD5JFw92MR+oqFmd426YZNHSjHK1",
  render_errors: [view: RealWorldWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: RealWorld.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "q+XBUBXB"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# git hooks
if Mix.env() != :prod do
  config :git_hooks,
    verbose: true,
    hooks: [
      pre_commit: [
        tasks: [
          "mix format --check-formatted",
          "mix credo"
        ]
      ],
      pre_push: [
        verbose: false,
        tasks: [
          "mix dialyzer",
          "mix credo",
          "mix test",
          "echo 'success!'"
        ]
      ]
    ]
end

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
