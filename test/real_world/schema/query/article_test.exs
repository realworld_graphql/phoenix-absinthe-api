defmodule RealWorld.Schema.Query.ArticleTest do
  use RealWorldWeb.ConnCase, async: true

  @query """
  query articles {
    articles {
      slug
    }
  }
  """
  test "articles field returns articles" do
    [article1, article2] = Enum.map(1..2, fn _ -> Factory.create_article() end)

    conn = build_conn()
    conn = get conn, "/api", query: @query

    assert json_response(conn, 200) == %{
             "data" => %{
               "articles" => [
                 %{"slug" => article1.slug},
                 %{"slug" => article2.slug}
               ]
             }
           }
  end

  @query """
  query Article($slug: String) {
    article(slug: $slug) {
      id
      slug
    }
  }
  """
  test "requesting article by slug returns the article" do
    slug = "test_slug"
    article = Factory.create_article(%{slug: slug})

    conn = build_conn()
    conn = get conn, "/api", query: @query, variables: %{"slug" => slug}

    assert json_response(conn, 200) == %{
             "data" => %{
               "article" => %{
                 "id" => to_string(article.id),
                 "slug" => article.slug
               }
             }
           }
  end
end
