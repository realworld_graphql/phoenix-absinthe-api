defmodule Factory do
  @moduledoc """
  Helper functions to create data for tests
  """

  def create_article(), do: create_article(%{})

  def create_article(params) do
    int = :erlang.unique_integer([:positive, :monotonic])

    params =
      Map.merge(
        %{
          slug: "slug_#{int}",
          title: "Some Title#{int}",
          description: "Descibes the article",
          body: "The body of the article"
        },
        params
      )

    save_article(params)
  end

  defp save_article(params) do
    %RealWorld.Content.Article{}
    |> RealWorld.Content.Article.changeset(params)
    |> RealWorld.Repo.insert!()
  end
end
