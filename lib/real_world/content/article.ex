defmodule RealWorld.Content.Article do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc """
  An Article on Conduit
  """

  schema "articles" do
    field :body, :string
    field :description, :string
    field :slug, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(article, attrs) do
    article
    |> cast(attrs, [:slug, :title, :description, :body])
    |> validate_required([:slug, :title, :description, :body])
  end
end
