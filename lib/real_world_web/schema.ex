defmodule RealWorldWeb.Schema do
  use Absinthe.Schema
  alias RealWorldWeb.Resolvers

  @moduledoc """
  The schema for the RealWorld GraphQL API
  """

  object :article do
    field :id, :id
    field :slug, non_null(:string)
    field :title, :string
    field :description, :string
    field :body, :string
  end

  query do
    field :articles, list_of(:article) do
      resolve(&Resolvers.Content.list_articles/3)
    end

    field :article, :article do
      arg(:slug, :string)
      resolve(&Resolvers.Content.get_article/3)
    end
  end
end
