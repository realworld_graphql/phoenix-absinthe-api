defmodule RealWorldWeb.Resolvers.Content do
  alias RealWorld.Content

  @moduledoc """
  Resolvers for content related types
  """

  def list_articles(_, _, _) do
    {:ok, Content.list_articles()}
  end

  def get_article(_, %{slug: slug}, _) do
    case Content.get_article_by_slug(slug) do
      nil -> {:error, "No article with this slug available: ${slug}"}
      article -> {:ok, article}
    end
  end
end
