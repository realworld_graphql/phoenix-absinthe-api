defmodule RealWorldWeb.Router do
  use RealWorldWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :api

    forward "/api", Absinthe.Plug, schema: RealWorldWeb.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: RealWorldWeb.Schema,
      interface: :simple
  end
end
